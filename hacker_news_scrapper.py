# pylint: disable=missing-module-docstring
# pylint: disable=unused-import
import smtplib
from datetime import datetime as now
import sqlite3
from sqlite3 import Error
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import os
from pprint import pprint as pp
import pandas as pd
import requests
from bs4 import BeautifulSoup
from dotenv import load_dotenv

URL = 'https://news.ycombinator.com/'
load_dotenv()
SERVER = 'smtp.gmail.com'
PORT = 587
FROM = os.getenv('FROM')
TO = os.getenv('TO')
PASS = os.getenv('PASS')


def html_soup(URL):
    """ 
    html parser.
    """
    req = requests.get(URL)
    soup = BeautifulSoup(req.content, 'html.parser')
    return soup

def scrape_site(html_placeHolder):
    """
    scraps site for relevant information.
    """
    DATA = {
        'headlines': [],
        'users': [],
        'source': []
    }
    for post in html_placeHolder.findAll('tr', {'class': 'athing'}):
        headline = post.find('a', {'class': 'storylink'})
        if headline:
            DATA['headlines'].append(headline.get_text())
        # site source
        source = post.find('span', {'class': 'sitestr'})
        if source:
            DATA['source'].append(source.get_text())
        else:
            DATA['source'].append("No URL")
    for post in html_placeHolder.findAll('td', {'class': 'subtext'}):
        user = post.find('a', {'class': 'hnuser'})
        if user:
            DATA['users'].append(user.get_text())
        else:
            DATA['users'].append("Nobody")
    DATA = list(
        zip(DATA['headlines'],
            DATA['users'],
            DATA['source']))
    # print(DATA)
    return DATA

def add_to_db(DATA, out_file='HN_CSV.csv', db='hn_db2.db'):
    """
    populates database from scraper results
    """
    df = pd.DataFrame(DATA, columns=['headlines', 'users', 'source'])
    df.to_csv(out_file)
    conn = sqlite3.connect(db)
    cur = conn.cursor()
    df.to_sql('HackerNews', conn, if_exists="replace", index=True)
    cur.execute('SELECT * FROM HackerNews').fetchall()
    cur.close()
    conn.close()

def do_the_thing():
    """
    this runs above functions to do the thing 
    (add top 30 stories to HackerNews table)
    """
    data = scrape_site(html_soup(URL))
    add_to_db(data)
    return data

email_me = do_the_thing()
print("#*#*"*7+"This is begining of prints"+"_@_@_"*7)
# pp(email_me)
print(type(email_me))
##########################
### EMAIL THAT SCRAPER ###
##########################

def convert_L_2_S(l_st):
    """
    converts input list to a string for emailing
    """
    string_4_email = ''
    for id, item in enumerate(l_st):
        string_4_email += ('<br> ' + str(id+1) + ' -||- ' +
                           str(item) + '\n' + ' <br> ')
    return string_4_email


pp(convert_L_2_S(email_me))


def make_message(str_ng):
    """
    Makes a message and sends it out
    """
    msg = MIMEMultipart()
    msg["Subject"] = "30 of HN top stories [Automated email]" + "" + \
        str(now.day) + '-' + str(now.month) + '-' + str(now.year)
    msg['From'] = FROM
    msg['To'] = TO
    msg.attach(MIMEText(str_ng, 'html'))
    print("~|~"*9 + 'Initiating Server' + "~|~"*9)
    server = smtplib.SMTP(SERVER, PORT)
    server.set_debuglevel(1)
    server.ehlo()
    server.starttls()
    server.login(FROM, PASS)
    server.sendmail(FROM, TO, msg.as_string())
    print("wait give me space")
    print('*-' * 10 + "Email sent" + '*-' * 10)
    server.quit()


def send_that_email():
    email_away = make_message(convert_L_2_S(email_me))
    return email_away


if __name__ == '__main__':
    do_the_thing()
    send_that_email()
