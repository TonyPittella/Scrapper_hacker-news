import requests
from flask import Flask, render_template, request, jsonify

import json
import pandas as pd
import sqlite3
from sqlite3 import Error


app = Flask(__name__)
HOST = '0.0.0.0'

DataBase = "hn_db2.db"
# conn = sqlite3.connect(DataBase)
# db = pd.read_sql("SELECT * FROM HackerNews", conn)
#cur = conn.cursor()


class Search_DB:
    """My database object"""
    DataBase = "hn_db2.db"

    def __init__(self):
        self.conn = sqlite3.connect(DataBase)
        self.cur = self.conn.cursor()
    # def search_head(self, HL_asked):
    #     self.cur.execute(f"SELECT * FROM HackerNews WHERE headlines LIKE '%{HL_asked}%' ")
    # def search_user(self, UR_asked):
    #     self.cur.execute(f"SELECT * FROM HackerNews WHERE users LIKE '%{UR_asked}%' ")
    # def search_source(self, SR_asked):
    #     self.cur.execute(f"SELECT * FROM HackerNews WHERE source LIKE '%{SR_asked}%' ")

    def show_all(self):
        self.cur.execute("SELECT * FROM HackerNews")

    def __del__(self):
        self.conn.close()


@app.route('/')
@app.route('/index', methods=['get', 'post'])
def index():
    """
    home html address.
    """
    search = Search_DB()

    return render_template('index.html')


@app.route('/search', methods=["POST"])
def search_db():
    """
    search bar.
    """
    if request.method == 'POST':
        var = request.form['search']
        conn = sqlite3.connect("hn_db2.db")
        cur = conn.cursor()
        res = cur.execute(
            f"SELECT * FROM HackerNews WHERE headlines LIKE '%' || ? || '%' ", (var,)).fetchall()
        if res == []:
            return "Sorry it appearers what you are looking for is not here.."
        else:
            return jsonify(res)


@app.route('/all', methods=['GET'])
def everthing():
    """
    gets multiple headlines.
    """
    conn = sqlite3.connect("hn_db2.db")
    cur = conn.cursor()
    cur.execute("SELECT * FROM HackerNews")
    return jsonify(cur.fetchmany(15))
    # return json.dumps(res)


@app.route('/headlines', methods=['GET'])
def headlines():
    """
    gets multiple headlines.
    """
    conn = sqlite3.connect("hn_db2.db")
    cur = conn.cursor()
    cur.execute("SELECT headlines FROM HackerNews")
    return jsonify(cur.fetchmany(15))


@app.route('/users', methods=['GET'])
def users():
    """
    gets multiple users who posted.
    """
    conn = sqlite3.connect("hn_db2.db")
    cur = conn.cursor()
    cur.execute("SELECT users FROM HackerNews")
    return jsonify(cur.fetchmany(15))


@app.route('/source', methods=['GET'])
def source_site():
    """
    gets multiple source sites.
    """
    conn = sqlite3.connect("hn_db2.db")
    cur = conn.cursor()
    cur.execute("SELECT source FROM HackerNews")
    return jsonify(cur.fetchmany(15))


if __name__ == '__main__':
    app.run(host="0.0.0.0", debug=True)
